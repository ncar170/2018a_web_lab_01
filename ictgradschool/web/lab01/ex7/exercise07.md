####**Carnivorous Animals:**

* Cat
* Dog
* Hawk
* Lion
* Fox
* Polar Bear

####**Herbivorous Animals:**

* Horse
* Cow
* Sheep
* Chicken
* Capybara
* Frog

####**Omnivorous Animals:**

* Raccoon
* Rat
* Fennec Fox